$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("End2End_Tests.feature");
formatter.feature({
  "line": 1,
  "name": "Automated End2End Tests",
  "description": "Description: Test End to End Integration",
  "id": "automated-end2end-tests",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Customer place an order by purchasing an item from search",
  "description": "",
  "id": "automated-end2end-tests;customer-place-an-order-by-purchasing-an-item-from-search",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "user is on Home Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "he search for \"dress\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "choose to buy the first item",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "moves to checkout from mini cart",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "enter personal details on checkout page",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "place the order",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinition.user_is_on_Home_Page()"
});
formatter.result({
  "duration": 7516966700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "dress",
      "offset": 15
    }
  ],
  "location": "StepDefinition.he_search_for(String)"
});
formatter.result({
  "duration": 17413852500,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.choose_to_buy_the_first_item()"
});
formatter.result({
  "duration": 24007242800,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.moves_to_checkout_from_mini_cart()"
});
formatter.result({
  "duration": 8784685200,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.enter_personal_details_on_checkout_page()"
});
formatter.result({
  "duration": 6471949600,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.place_the_order()"
});
formatter.result({
  "duration": 26743900,
  "status": "passed"
});
});