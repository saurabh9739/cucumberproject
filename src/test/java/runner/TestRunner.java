package test.java.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src\\test\\java\\feature",
		glue= {"test\\java\\stepDefinition"},
		monochrome = true,
	    plugin  = {"pretty","html:target/htmlReport"}
		)

public class TestRunner {
}
