package test.java.stepDefinition;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class StepDefinition {
	
	WebDriver driver;
	@Given("^user is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Saura\\eclipse-workspace\\CucumberProject\\src\\main\\java\\drivers\\chromeDriver\\chromedriver.exe");
	    
		ChromeOptions capability = new ChromeOptions();
		capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
		driver = new ChromeDriver(capability);
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.get("https://www.shop.demoqa.com");	    
	}

	@When("^he search for \"([^\"]*)\"$")
	public void he_search_for(String arg1) throws Throwable {
		driver.findElement(By.xpath("//*[@id='details-button']")).click();
		driver.navigate().to("https://shop.demoqa.com/?s=" + arg1 + "&post_type=product");
	}

	@When("^choose to buy the first item$")
	public void choose_to_buy_the_first_item() throws Throwable {
		List<WebElement> items = driver.findElements(By.cssSelector(".noo-product-inner"));
		items.get(0).click();

		WebElement chooseColorOptionDropDown = driver.findElement(By.xpath("//*[@id='pa_color']"));
		Select selectColor = new Select(chooseColorOptionDropDown);
		selectColor.selectByValue("white");
		System.out.println("White selected");
		WebElement chooseSizeOptionDropDown = driver.findElement(By.xpath("//*[@id='pa_size']"));
		Select selectSize = new Select(chooseSizeOptionDropDown);
		selectSize.selectByValue("medium");
		System.out.println("White selected");
		
		WebElement addToCart = driver.findElement(By.cssSelector("button.single_add_to_cart_button"));
		addToCart.click();		
	}

	@When("^moves to checkout from mini cart$")
	public void moves_to_checkout_from_mini_cart() throws Throwable {
		WebElement cart = driver.findElement(By.cssSelector(".cart-button"));
		cart.click();

		WebElement continueToCheckout = driver.findElement(By.cssSelector(".checkout-button.alt"));
		continueToCheckout.click();
	}

	@When("^enter personal details on checkout page$")
	public void enter_personal_details_on_checkout_page() throws Throwable {
		Thread.sleep(5000);
		WebElement firstName = driver.findElement(By.cssSelector("#billing_first_name"));
		firstName.sendKeys("saurabh");

		WebElement lastName = driver.findElement(By.cssSelector("#billing_last_name"));
		lastName.sendKeys("Tripathi");

		WebElement streetAddress = driver.findElement(By.xpath("//*[@id='billing_address_1']"));
		streetAddress.sendKeys("Street 1");
		
		WebElement city = driver.findElement(By.xpath("//*[@id='billing_city']"));
		city.sendKeys("City 1");
		
		WebElement termsAndConditionCheckBox = driver.findElement(By.xpath("//*[@id='terms']"));
		termsAndConditionCheckBox.click();
		
		WebElement state = driver.findElement(By.xpath("//*[@id='billing_state']"));
		Select selectState = new Select(state);
		selectState.selectByValue("DL");
		
		WebElement zipcode = driver.findElement(By.xpath("//*[@id='billing_postcode']"));
		zipcode.sendKeys("110096");
		
		WebElement phone = driver.findElement(By.cssSelector("#billing_phone"));
		phone.sendKeys("07438862327");
		
		WebElement emailAddress = driver.findElement(By.cssSelector("#billing_email"));
		emailAddress.sendKeys("test@yopmail.com");		
	}

	@When("^place the order$")
	public void place_the_order() throws Throwable {
		//WebElement acceptTC = driver.findElement(By.cssSelector("#terms.input-checkbox"));
		//acceptTC.click();
		
		//WebElement termsAndConditionCheckBox = driver.findElement(By.xpath("//*[@id='terms']"));
		//termsAndConditionCheckBox.click();
		
		//WebElement termsAndConditionCheckBox = driver.findElement(By.xpath("//*[contains(text(),'I have read and agree to the website ')] "));
		//termsAndConditionCheckBox.click();

		WebElement placeOrder = driver.findElement(By.cssSelector("#place_order"));
		placeOrder.submit();
	}
}
